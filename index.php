<!DOCTYPE html>
<html>
<?
include 'ajax.php';

function printjs() {
	echo "
    <script type='text/javascript'>
    $(document).ready(function() {
        getLatestRuns();
        getWeekTotal();
        getYearTotal();
        getLifetimeTotal();
    });
    function getWeekTotal() {
        var weekStart = moment().startOf('week').format('YYYY-MM-DD');
        $.getJSON('ajax.php?getTotalSince=' + weekStart, function(data) {
            var html = 'Week to date: ' + data[0] + ' mi';
            $('#rw_week').html(html);
        });

    }
    function getYearTotal() {
        var weekStart = moment().startOf('year').format('YYYY-MM-DD');
        $.getJSON('ajax.php?getTotalSince=' + yearStart, function(data) {
            var html = 'Year to date: ' + data[0] + ' mi';
            $('#rw_year').html(html);
        });

    }
    function getLifetimeTotal() {
        var lifeStart = moment().subtract('years', 5).unix();
        $.getJSON('ajax.php?getTotalSince=' + lifeStart, function(data) {
            var html = 'Lifetime: ' + data[0] + ' mi';
            $('#rw_lifetime').html(html);
        });

    }
    function getLatestRuns() {
        $.getJSON('ajax.php?getRuns=3', function(data) {
            var dist = data[0][1];
            var duration = moment.duration(parseInt(data[0][2])).asMinutes();
            var date = moment(parseInt(data[0][3])).from(moment());

            var html = '<table><tr><td><div class=\"distance\"><span class=\"miles\">' + dist + '</span><br>miles</div></td>';
            html += '<td><div class=\"duration\">' + duration + ' minutes</div>';
            html += '<div class=\"date\">' + date + '</div></td></tr></table>';
            html += '<img src=\"images/run.png\">'
            $('#rw_last').html(html);
        });
    }
        
    </script>
    ";
}

?>
<head>
    <title>Running Widget Editor</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="js/datepicker/css/datepicker.css" rel="stylesheet" media="screen">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/moment.min.js"></script>
    <script src="js/datepicker/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            loadRuns()
            $('#form-date').value = moment();
            t = moment().format('MM/DD/YYYY')
            y = moment().subtract('days', 1).format('MM/DD/YYYY')
            d = $('#form-date').datepicker().on('changeDate', function(ev) {
                if ($('#form-date').val() == t) {
                    $('#form-btn-today').addClass('active');
                    $('#form-btn-yest').removeClass('active');
                    $('#form-date').removeClass('info');
                }
                else if ($('#form-date').val() == y) {
                    $('#form-btn-today').removeClass('active');
                    $('#form-btn-yest').addClass('active');
                    $('#form-date').removeClass('info');
                }
                else {
                    $('#form-btn-today').removeClass('active');
                    $('#form-btn-yest').removeClass('active');
                    $('#form-date').addClass('info');
                }
            });
            d.datepicker('setValue', t)
            

            $('#newRunForm').submit(function(event) {
                event.preventDefault();
                var posting = $.post(
                    'ajax.php', {
                        dist: $('#form-dist').val(),
                        duration: $('#form-dur').val(),
                        date: moment($('#form-date').val(), "MM-DD-YYYY").format('YYYY-MM-DD')
                    });
                posting.done(function(data) {
                    if (data) {
                        addMessage("Run successfully added!", "alert-success");
                        loadRuns();
                        $('#form-dist').val("");
                        $('#form-dur').val("");
                        // Reload widget
                        $('#iphrame').attr('src', $('#iphrame').attr('src'));
                    }
                    else
                        addMessage("There was an error adding your run. Sorry", "alert-error");
                });

            });


        });
        function deleteRun(run_id) {
            if (confirm("Are you sure you want to delete this run?")) {
                var posting = $.post(
                    'ajax.php', {
                        delete: run_id
                    });
                posting.done(function(data) {
                    if (data) {
                        loadRuns();
                        addMessage("Run deleted successfully!", "alert-success");
                        $('#iphrame').attr('src', $('#iphrame').attr('src'));
                    }
                    else {
                        addMessage("Error deleting run", "alert-error");
                    }
                });
            }
        }
        function loadRuns() {
            var posting = $.post(
                'ajax.php', {
                    showAllRuns: 1
                });
            posting.done(function(data) {
                $('#allRuns').html(data);
                $('.rundate').each(function() { 
                    this.innerHTML = moment(this.innerHTML).format('ddd, MMM Do');
                });
                $('.runduration').each(function() { 
                    this.innerHTML  += " min";
                });
            });

        }
        function addMessage(msg, type) {
            var finalMsg = "<div class='alert "+ type +"'>" 
            finalMsg += "<button type='button' class='close' data-dismiss='alert'>&times;</button>"
            finalMsg += msg + "</div>";
            $('#message').html(finalMsg)
        }
    </script>
</head>
<body>
    <div class="container">
        <h1 class="page-header">Running Widget Editor</h1>
        <div id='message' class="row-fluid">
        </div>
        <div class="row-fluid">
            <h3>Add run</h3>
            <form id='newRunForm' class="form-inline">
                <input type="number" step="any" name="dist" id="form-dist" class="input-medium" placeholder="Distance (mi)" required>
                <input type="number" step="any" name="duration" id="form-dur" class="input-medium" placeholder="Duration (min)" required>
                <div class="btn-group" data-toggle="buttons-radio">
                  <button type="button" onClick="d.datepicker('setValue', t)" id="form-btn-today" class="btn active">Today</button>
                  <button type="button" onClick="d.datepicker('setValue', y)" id="form-btn-yest" class="btn">Yesterday</button>
                </div>
                <input type="text" name="date" id="form-date" class="input-small" required>
                <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
        <div class="row-fluid">
            <div class="span8">
                <h3>Previous Runs</h3>
                <div id='allRuns'>
                </div>
            </div>
            <div class="span4">
                <h3>Widget preview:</h3>
		<div id="running-widget"></div>
		<link rel="stylesheet" type="text/css" href="css/style.css"></link>
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Raleway:400,200"></link>
<!--
-->
		<script src="js/moment.min.js"></script>
		<script src="widget.js"></script>
		<h3>IFrame preview</h3>
		<!--<iframe id="iphrame" height="300" src="http://sitesmiths.us/run/wtest.php"></iframe>-->
            </div>
        </div>
    </div>
</body>
</html>
