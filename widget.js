$(document).ready(function() {
	printWidget();
	getLatestRuns();
	getWeekTotal();
	getYearTotal();
	getLifetimeTotal();
});
function getWeekTotal() {
	var weekStart = moment().format('YYYY-MM-DD');
	$.getJSON('ajax.php?getTotalSince=' + weekStart, function(data) {
        var html = 'Week to date: ' + data[0] + ' mi';
        $('#rw_week').html(html);
    });

}
function getYearTotal() {
	var yearStart = moment().startOf('year').format('YYYY-MM-DD');
	$.getJSON('ajax.php?getTotalSince=' + yearStart, function(data) {
        var html = 'Year to date: ' + data[0] + ' mi';
        $('#rw_year').html(html);
    });

}
function getLifetimeTotal() {
	var lifeStart = moment().subtract('years', 5).format('YYYY-MM-DD');
	$.getJSON('ajax.php?getTotalSince=' + lifeStart, function(data) {
        var html = 'Lifetime: ' + data[0] + ' mi';
        $('#rw_lifetime').html(html);
    });

}
function getLatestRuns() {
	$.getJSON('ajax.php?getRuns=3', function(data) {
		var dist = parseFloat(data[2][1]).toFixed(1);
		var duration = parseInt(data[2][2]);
		var date = moment(data[2][3]);

		if (date.date() == moment().date()) {
            date = 'Today';
		}
		else {
            date = moment(data[2][3]).from(moment());
		}

		var html = '<table><tr><td><div class=\"distance\"><span class=\"miles\">' + dist + '</span><br>miles</div></td>';
		html += '<td><div class=\"duration\">' + duration + ' minutes</div>';
		html += '<div class=\"date\">' + date + '</div></td></tr></table>';
		html += '<img src=\"images/run.png\">'
		$('#rw_last').html(html);
	});
}


function printWidget() {
	var html = "<div id='rw_wrapper'> <!--<div id='rw_title' class='title odd' >Latest Runs</div>--> <div id='rw_last' class='entry latest even'> <img src='images/ajax-loader.gif'/> </div> <div id='rw_week' class='entry odd'></div> <div id='rw_year' class='entry even'></div> <div id='rw_lifetime' class='entry odd'></div> </div>";
	$('#running-widget').html(html);
}


