<?
try {
    $file_db = new PDO('sqlite:running.db');
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    /*
    $file_db->exec("CREATE TABLE IF NOT EXISTS runs (
        id INTEGER PRIMARY KEY,
        distance NUMERIC,
        duration INTEGER,
        date INTEGER
        )");
     */

    $mem_db = null;
}
catch(PDOException $e) {
    echo $e->getMessage();
}

// Insert a new run into the database
if (isset($_POST['dist']) && isset($_POST['duration']) && isset($_POST['date']) ) {
    $dist = mysql_escape_string($_POST['dist']);
    $duration = mysql_escape_string($_POST['duration']);
    $date = mysql_escape_string($_POST['date']);
    insertRun($dist, $duration, $date);

}
else if (isset($_POST['delete'])) {
    deleteRun(mysql_escape_string($_POST['delete']));
}
else if (isset($_GET['getRuns'])) {
    getRuns(mysql_escape_string($_GET['getRuns']));
}
else if (isset($_GET['getTotalSince'])) {
    getTotalSince(mysql_escape_string($_GET['getTotalSince']));
}
else if (isset($_POST['showAllRuns'])) {
    showAllRuns();
}

function getTotalSince($startDate) {
    if (preg_match("/^\d{4}-[0|1][0-9]-[0-3][0-9]$/", $startDate)) {
        global $file_db;
        $array = array();
        $query = "SELECT TOTAL(distance) FROM runs WHERE date >= '" . $startDate . "'";
        $result = $file_db->query($query);
        foreach ($result as $row) {
            $array = $row;
        }
        echo json_encode($array);
    }
    else {
        echo "Invalid date";
    }
}

function insertRun($dist, $duration, $date) {
    global $file_db;
    $insert = "INSERT INTO runs (distance, duration, date)
                VALUES (:distance, :duration, :date)";
    $query = $file_db->prepare($insert);
    $query->bindParam(':distance', $dist);
    $query->bindParam(':duration', $duration);
    $query->bindParam(':date', $date);
    echo $query->execute();
}

function deleteRun($id) {
    global $file_db;
    $query = "DELETE FROM runs where id = $id";
    if ($file_db->query($query))
        echo "1";
    else
        echo "0";
}

function getRuns($limit) {
    global $file_db;
    $array = array();
    $result = $file_db->query('SELECT * FROM runs ORDER BY date DESC LIMIT ' . $limit);
    foreach ($result as $row) {
        array_unshift($array, $row);
    }
    echo json_encode($array);
}

function showAllRuns() {
    global $file_db;
    $result = $file_db->query('SELECT * FROM runs ORDER BY date DESC');
    $output = "<table class='table table-hover'><tr><th>distance</th><th>duration</th><th>date</th><th>delete</th></tr>";
    foreach ($result as $row) {
        $output .= "<tr>";
        // $output .= "<td>" . $row['id'] . "</td>";
        $output .= "<td>" . $row['distance'] . " mi</td>";
        $output .= "<td class='runduration'>" . $row['duration'] . "</td>";
        $output .= "<td class='rundate'>" . $row['date'] . "</td>";
        $output .= "<td>" . " <button type='button' class='close' onClick='deleteRun(".$row['id'].")'>&times;</button>" . "</td>";
        $output .= "</tr>";
    }
    $output .= "</table>";
    echo $output;

}

function validdate($datestr) {

}


?>
